
clear ; close all; clc

num_labels = 4;         

%Load training data and training labels
X = load('train_1');
m = size(X, 1);
y = load('train_label_1');

fprintf('\nTraining One-vs-All Logistic Regression...\n')
fprintf('Lambda = 10\n');

lambda = 10000;
[all_theta] = oneVsAll(X, y, num_labels, lambda);

fprintf('All %d decision boundaries or theta bases found', num_labels);

%Predicting the training data classes
pred = predictOneVsAll(all_theta, X);

save "training_prediction_1.txt" pred;

fprintf('\nTraining Set Accuracy: %f\n', mean(double(pred == y)) * 100);

%Predicting the testing data
fprintf('Now running the testing data on the learned model...\n');
X_Test = load('test_1');
pred_Test = predictOneVsAll(all_theta, X_Test);
save "testing_prediction_1.txt" pred_Test;
fprintf('Program terminated.\n');


