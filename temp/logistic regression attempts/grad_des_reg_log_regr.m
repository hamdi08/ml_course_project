function [theta, J_history] = grad_des_reg_log_regr(X, y, theta, alpha, lambda, num_iters)
m = length(y);
J_history = zeros(num_iters, 1);
for iter = 1:num_iters
	temp = theta;
	z = X * theta;
	s = sigmoid(z);
	dev = s-y;
	mul_dev = X' * dev;
	grad = (1/m) * mul_dev;
	temp = (1 - (alpha*lambda)/m)*theta - alpha * grad;
	theta = temp;
	%J_history(iter) = computeCost(X, y, lambda, theta);
	%if iter mod 100 == 0
		%fprintf('J = %f\n', J_history(iter));
		%end

end
