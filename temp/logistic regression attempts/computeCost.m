function J = computeCost(X, y, lambda, theta)

m = length(y);
n = size(X)(2)-1;
J = 0;
sum_reg = 0;
for j = 2:n+1
	sum_reg = sum_reg + theta(j)^2;
	end;

z = X * theta; % z high value
s = sigmoid(z); %S --> all 1
first_term = -(1/m) * sum( y.*log(s) + (1 - y).*log(1 - s) ); % log(1-s) --> Inf

J = first_term + (lambda/(2*m)) * sum_reg;
J

end
