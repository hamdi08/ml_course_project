clear ; close all; clc
X = load('train_1');
y = load('train_label_1');
m = size(X)(1);
n = size(X)(2);
X = [ones(m,1), X];
k = 4;
theta_base = zeros(n+1, k);
iterations = 3000;
alpha = 0.003;
lambda = 1000;

for i=2:n+1
	mean_value = mean(X(:,i));
	sd_value = std(X(:,i));
	%sd_value
	for j=1:m
		if sd_value == 0
			X(j,i) = 0;
		else
			X(j,i) = (X(j,i)-mean_value)/sd_value;
		end
	end
end

for i=1:k
	%fprintf('alive');
	theta_base(:, i) = grad_des_reg_log_regr(X, y, theta_base(:, i), alpha, lambda, iterations);
	end

train_predictions = zeros(m, 1);
for i = 1:m
	scores = zeros(k,1);
	for j = 1:k
		theta_t_x = X(i, :)*theta_base(:,j);
		h_theta_t_x = sigmoid(theta_t_x);
		scores(j) = h_theta_t_x;
		fprintf('Training example: %d --> score on DB %d = %f\n',i,j,scores(j));
		end
	maxIndex = find(scores == max(scores));
	if(length(maxIndex)>1)
		maxIndex = maxIndex(1);
		end
	train_predictions(i) = maxIndex;
end

save prediction_on_train_data.txt train_predictions;

fprintf('Train Accuracy: %f\n', mean(double(train_predictions == y)) * 100);

	
