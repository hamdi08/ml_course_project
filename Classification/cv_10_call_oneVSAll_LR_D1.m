clear ; close all; clc
%Not showing the warnings
do_braindead_shortcircuit_evaluation (0);

k = 10; % 10 fold cross validation
lambda = 1000;
%Load training data and training labels
X = load('train_1');
m = size(X, 1);
y = load('train_label_1');

labels = unique(y);
num_labels = max(labels);

idx = randperm(m);
bin_size = floor(m/k);
bin_idx = zeros(bin_size, k);

correct_count = 0;
for i = 1:k
	bin_idx(:, i) = idx( (i-1)*bin_size+1 : i*bin_size );
	%bin_idx(:, i)
	test_idx = bin_idx(:, i);
	test_data = X(test_idx, :);
	%size(test_data)
	test_labels = y(test_idx);
	train_idx = setdiff(idx, test_idx);
	train_data = X(train_idx, :);
	train_labels = y(train_idx);
	[all_theta] = oneVsAll(train_data, train_labels, num_labels, lambda);
	pred = predictOneVsAll(all_theta, test_data);
	correct_count = correct_count + sum(pred == test_labels);
	end;
mean_accuracy = (correct_count/m)*100;


fprintf('\nTraining One-vs-All Logistic Regression in train data 1...\n');
fprintf('Number of classes: %d\n',num_labels);
fprintf('Lambda = %f\n', lambda);
fprintf('Number of training examples: %d\n', m);
fprintf('Number of dimensions: %d\n', size(X, 2));
fprintf('After %d fold cross validation: mean_accuracy on training data 2: %f\n',k, mean_accuracy);


%Predicting the testing data
fprintf('Now running the testing data on the learned model...\n');
X_Test = load('test_1');
fprintf('Number of testing examples: %d\n', size(X_Test, 1));
pred_Test = predictOneVsAll(all_theta, X_Test);
save "testing_prediction_1.txt" pred_Test;
fprintf('Testing predictions written in file. \n');
fprintf('Program terminated.\n');
	
